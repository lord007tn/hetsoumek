import React, { Fragment, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
//Redux
import { Provider } from 'react-redux'
import store from './store'

import Header from './components/components/Header'
import Home from './components/layouts/Home'
import Footer from './components/components/Footer'
import Login from './components/layouts/Login'
import Register from './components/layouts/Register'
import setAuthToken from './utils/setAuthToken'
import { loadUser } from './actions/auth'
import ProductsHome from './components/layouts/ProductsHome'
import Contact from './components/layouts/Contact'
import './App.css'
import AdminDashboardRoutes from './routing/AdminDashboardRoutes'
import ProductDetails from './components/layouts/ProductDetails'
import Profile from './components/layouts/Profile'
import Deals from './components/layouts/Deals'
import Categories from './components/layouts/Categories'
import ProductsByCategory from './components/layouts/ProductsByCategory'
import NotFound from './components/components/NotFound'
//import 'rsuite/dist/styles/rsuite-default.css'
if (localStorage.token) {
  setAuthToken(localStorage.token)
}
const App = () => {
  useEffect(() => {
    store.dispatch(loadUser())
  }, [])
  return (
    <Provider store={store}>
      <Fragment style={{height:'100vh'}}>
        <Router>
          <Header />
          <Switch  >
            <Route exact path="/" component={Home} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/products" component={ProductsHome} />
            <Route exact path="/deals" component={Deals} />
            <Route exact path="/categories" component={Categories} />
            <Route exact path="/categories/:category" component={ProductsByCategory} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/:id/details" component={ProductDetails} />
            <Route exact path="/userprofile" component={Profile} />
            <Route  component={AdminDashboardRoutes} />
            <Route component={NotFound} />
          </Switch>
          <Footer />
        </Router>
      </Fragment>
    </Provider>
  )
}

export default App
