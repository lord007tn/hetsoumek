import {
    CREATE_BID,
    BID_ERROR,
    GET_BID,
    GET_BIDS
} from './types'
import axios from 'axios';

export const createBid = (data, productID) => async dispatch =>{
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    try {
        const res = await axios.post(`/api/bid/${productID}/create`, data, config)
        dispatch({
            type: CREATE_BID,
        })
    } catch (err) {
        dispatch({
            type: BID_ERROR,
            payload: err
        })
    }
}
export const getBids = () => async dispatch => {

    try {
        const res = await axios.get('/api/bid/')
        dispatch({
            type:GET_BIDS,
            payload: res.data.bids
        })
    } catch (err) {
        dispatch({
            type:BID_ERROR,
            payload: err
        })
    }
}

export const getBid = (id) => async dispatch => {

    try {
        const res = await axios.get(`/api/bid/${id}`)
        dispatch({
            type:GET_BID,
            payload: res.data.bid
        })
    } catch (err) {
        dispatch({
            type:BID_ERROR,
            payload: err
        })
    }
}