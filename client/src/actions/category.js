import axios from 'axios';
import {
    GET_CATEGORIES,
    GET_CATEGORY,
    CATEGORY_ERROR,
    CREATE_CATEGORY,
    UPDATE_CATEGORY,
    DELETE_CATEGORY
} from './types'

export const getCategories = () => async dispatch => {

    try {
        const res = await axios.get('/api/category/')
        dispatch({
            type:GET_CATEGORIES,
            payload: res.data.categories
        })
    } catch (err) {
        dispatch({
            type:CATEGORY_ERROR,
            payload: err
        })
    }
}

export const getCategory = (id) => async dispatch => {

    try {
        const res = await axios.get(`/api/category/${id}`)
        dispatch({
            type:GET_CATEGORY,
            payload: res.data.category
        })
    } catch (err) {
        dispatch({
            type:CATEGORY_ERROR,
            payload: err
        })
    }
}

export const createCategory = (data) => async dispatch =>{
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    try {
        const res = await axios.post(`/api/category/create`, data, config)
        dispatch({
            type: CREATE_CATEGORY,
        })
    } catch (err) {
        dispatch({
            type: CATEGORY_ERROR,
            payload: err
        })
    }
}

export const updateCategory = (data, id) => async dispatch =>{
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }
    try {
        const res = await axios.put(`/api/category/${id}/update`, data, config)
        dispatch({
            type: UPDATE_CATEGORY,
        })
    } catch (err) {
        dispatch({
            type: CATEGORY_ERROR,
            payload: err
        })
    }
}

export const deleteCategory = (id) => async dispatch =>{

    try {
        const res = await axios.delete(`/api/category/${id}/delete`)
        dispatch({
            type:DELETE_CATEGORY,
            payload: id
        })
    } catch (err) {
        dispatch({
            type:CATEGORY_ERROR,
            payload: err
        })
    }
}