import axios from 'axios';
import {
    GET_PRODUCTS,
    GET_PRODUCT,
    PRODUCT_ERROR,
    CREATE_PRODUCT,
    GET_TODAY_PRODUCTS,
    UPDATE_PRODUCT,
    DELETE_PRODUCT
} from './types'

export const getProducts = () => async dispatch => {

    try {
        const res = await axios.get('/api/product/')
        dispatch({
            type:GET_PRODUCTS,
            payload: res.data.products
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}
export const getTodayProducts = () => async dispatch => {

    try {
        const res = await axios.get('/api/product/today')
        dispatch({
            type:GET_TODAY_PRODUCTS,
            payload: res.data.products
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}
export const getProductsByCategory = (name) => async dispatch => {

    try {
        const res = await axios.get(`/api/product/${name}/products`)
        dispatch({
            type:GET_PRODUCTS,
            payload: res.data.products
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}
export const getProduct = (id) => async dispatch => {

    try {
        const res = await axios.get(`/api/product/${id}`)
        dispatch({
            type:GET_PRODUCT,
            payload: res.data.product
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}

export const createProduct = (data) => async dispatch =>{

    const config = {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }
    console.log(data)
    try {
        const res = await axios.post('/api/product/create', data, config)
        dispatch({
            type:CREATE_PRODUCT
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}

export const updateProduct = (id, data) => async dispatch =>{

    const config = {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }
    console.log(data)
    try {
        const res = await axios.put(`/api/product/${id}/update`, data, config)
        dispatch({
            type:UPDATE_PRODUCT
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}

export const deleteProduct = (id) => async dispatch =>{

    try {
        const res = await axios.delete(`/api/product/${id}/delete`)
        dispatch({
            type:DELETE_PRODUCT,
            payload: id
        })
    } catch (err) {
        dispatch({
            type:PRODUCT_ERROR,
            payload: err
        })
    }
}