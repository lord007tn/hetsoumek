//Auth
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAIL = 'REGISTER_FAIL'
export const USER_LOADED = 'USER_LOADED'
export const USER_LOADING = 'USER_LOADING'
export const AUTH_ERROR = 'AUTH_ERROR'
export const LOGIN_FAIL = 'LOGIN_FAIL'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGOUT = 'LOGOUT'
//Alert
export const SET_ALERT = 'SET_ALERT'
export const REMOVE_ALERT = 'REMOVE_ALERT'
//Loading Screen
export const SET_LOADING = 'SET_LOADING'
export const REMOVE_LOADING = 'REMOVE_LOADING'
//Category
export const GET_CATEGORIES = 'GET_CATEGORIES'
export const CATEGORY_ERROR = 'CATEGORY_ERROR'
export const GET_CATEGORY = 'GET_CATEGORY'
export const CREATE_CATEGORY= 'CREATE_CATEGORY'
export const UPDATE_CATEGORY = 'UPDATE_CATEGORY'
export const DELETE_CATEGORY = 'DELETE_CATEGORY'
//Product
export const PRODUCT_ERROR = 'PRODUCT_ERROR'
export const GET_PRODUCT = 'GET_PRODUCT'
export const GET_PRODUCTS = 'GET_PRODUCTS'
export const GET_TODAY_PRODUCTS = 'GET_TODAY_PRODUCTS'
export const CREATE_PRODUCT = 'CREATE_PRODUCT'
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT'
export const DELETE_PRODUCT = 'DELETE_PRODUCT'
//User
export const GET_USERS = 'GET_USERS'
export const USER_ERROR = 'USER_ERROR'
export const GET_USER = 'GET_USER'
export const CREATE_USER= 'CREATE_USER'
export const UPDATE_USER = 'UPDATE_USER'
export const DELETE_USER = 'DELETE_USER'
//Bid
export const GET_BIDS = 'GET_BIDS'
export const BID_ERROR = 'BID_ERROR'
export const GET_BID = 'GET_BID'
export const CREATE_BID= 'CREATE_BID'
export const UPDATE_BID = 'UPDATE_BID'
export const DELETE_BID = 'DELETE_BID'