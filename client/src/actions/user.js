import axios from 'axios';
import {
    GET_USERS,
    GET_USER,
    USER_ERROR
} from './types'
export const getUsers = () => async dispatch => {

    try {
        const res = await axios.get('/api/user/')
        console.log(res.data)
        dispatch({
            type:GET_USERS,
            payload: res.data.users
        })
    } catch (err) {
        dispatch({
            type:USER_ERROR,
            payload: err
        })
    }
}

export const getUser = (id) => async dispatch => {

    try {
        const res = await axios.get(`/api/user/${id}`)
        dispatch({
            type:GET_USER,
            payload: res.data.user
        })
    } catch (err) {
        dispatch({
            type:USER_ERROR,
            payload: err
        })
    }
}
