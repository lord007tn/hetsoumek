import React, { Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import Spinner from '../components/Spinner'
const CategoryItem = ({ category }) => {
  return (
    <Fragment>
    <div class="flex flex-col bg-white px-8 py-6 max-w-sm mx-auto rounded-lg shadow-lg">
    <Link to={`/categories/${category.categoryName}`}>
        <div class="flex justify-center items-center">
            <Link class="px-2 py-1 bg-gray-600 text-sm text-green-100 rounded" to={`/categories/${category.categoryName}`}>{category.categoryName}</Link>
        </div>
        <div class="mt-4">
            <Link class="text-lg text-gray-700 font-medium" to={`/categories/${category.categoryName}`}>{category.about}</Link>
        </div>
        </Link>
        </div>
    </Fragment>
  )
}
CategoryItem.prototype = {
  category: PropTypes.object.isRequired,
}
export default CategoryItem
