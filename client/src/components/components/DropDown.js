import React, { Fragment } from 'react'
import { createPopper } from '@popperjs/core'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
const Dropdown = ({ color, categories }) => {
  const state = useSelector((state) => state)
  console.log('My state', state.category.categories)
  // dropdown props
  const [dropdownPopoverShow, setDropdownPopoverShow] = React.useState(false)
  const btnDropdownRef = React.createRef()
  const popoverDropdownRef = React.createRef()
  const openDropdownPopover = () => {
    new createPopper(btnDropdownRef.current, popoverDropdownRef.current, {
      placement: 'bottom-start',
    })
    setDropdownPopoverShow(true)
  }
  const closeDropdownPopover = () => {
    setDropdownPopoverShow(false)
  }
  // bg colors
  let bgColor
  color === 'white'
    ? (bgColor = 'bg-gray-800')
    : (bgColor = 'bg-' + color + '-500')
  return (
    <Fragment>
      <button
        className="font-bold"
        style={{ transition: 'all .15s ease' }}
        type="button"
        ref={btnDropdownRef}
        onClick={() => {
          dropdownPopoverShow ? closeDropdownPopover() : openDropdownPopover()
        }}
      >
        {color === 'white' ? 'Catégories' : color + ' Dropdown'}
      </button>
      <div
        ref={popoverDropdownRef}
        className={
          (dropdownPopoverShow ? 'block ' : 'hidden ') +
          (color === 'white' ? 'bg-white ' : bgColor + ' ') +
          'text-base z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1'
        }
        style={{ minWidth: '12rem' }}
      >
        {state.category.categories.map((elm, i) => {
          if(i<6){
            return (
            <Link
              to={`/categories/${elm.categoryName}`}
              className={
                'text-sm py-2 px-4 font-normal block w-full whitespace-no-wrap bg-transparent hover:bg-blue-700 hover:text-white' +
                (color === 'white' ? ' text-gray-800' : 'text-white')
              }
              onClick={closeDropdownPopover}
            >
              {elm.categoryName}
            </Link>
          )
          }
        })}
      </div>
    </Fragment>
  )
}

export default function DropdownRender() {
  return (
    <Fragment>
      <Dropdown color="white" />
    </Fragment>
  )
}
