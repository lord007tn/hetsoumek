import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logout } from "../../actions/auth";
import DropdownRender from "./DropDown";
const Header = ({ logout, auth }) => {
  const clientLinks = (
    <div
      className="menu hidden w-full flex-grow md:flex lg:items-center lg:w-auto lg:px-3 px-8"
      id="nav-content"
    >
      <div className="text-md font-bold text-blue-700 lg:flex-grow">
        <Link
          to="/"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Acceuil
        </Link>
        <Link
          to="/products"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Enchers En Cours
        </Link>
        <Link to="/deals" className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          Super Offres
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2" 
         to="/userprofile">
          Mon Profile 
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          <DropdownRender />
        </Link>
      </div>

      <div className="relative mx-auto text-gray-600 lg:block hidden mr-2">
        <input
          className="border-2 border-gray-300 bg-white h-10 pl-2 pr-8 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Search"
        />
        <button
          type="submit"
          className="absolute right-0 top-0 mt-3 mr-2"
        ></button>
      </div>
        <Link
          to="/"
          onClick={logout}
          className=" block text-md px-4 py-2 rounded text-blue-700 font-bold hover:text-white mt-4 hover:bg-blue-700 lg:mt-0"
        >
          Se Déconnecter
        </Link>
    </div>
  );
  const sellerLinks = (
    <div
      className="menu hidden w-full lg:block  flex-grow lg:flex lg:items-center lg:w-auto lg:px-3 px-8"
      id="nav-content"
    >
      <div className="text-md font-bold text-blue-700 lg:flex-grow">
        <Link
          to="/"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Acceuil
        </Link>
        <Link
          to="/products"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Enchers En Cours
        </Link>
        <Link to="/deals" className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          Super Offres
        </Link>
        <Link
          to="/admin/products"
          className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Tableau de bord
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          <DropdownRender />
        </Link>
      </div>

      <div className="relative mx-auto text-gray-600 lg:block hidden mr-2">
        <input
          className="border-2 border-gray-300 bg-white h-10 pl-2 pr-8 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Search"
        />
        <button
          type="submit"
          className="absolute right-0 top-0 mt-3 mr-2"
        ></button>
      </div>
        <Link
          to="/"
          onClick={logout}
          className=" block text-md px-4 py-2 rounded text-blue-700 font-bold hover:text-white mt-4 hover:bg-blue-700 lg:mt-0"
        >
          Se Déconnecter
        </Link>
    </div>
  );
  const visitorLinks = (
    <div
      className="menu sm:hidden w-full flex-grow lg:flex lg:items-center lg:w-auto lg:px-3 px-8"
      id="nav-content"
    >
      <div className="text-md font-bold text-blue-700 lg:flex-grow">
        <Link
          to="/"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Acceuil
        </Link>
        <Link
          to="/products"
          className="block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2"
        >
          Enchers En Cours
        </Link>
        <Link to="/deals" className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          Super Offres
        </Link>
        <Link className=" block mt-4 lg:inline-block lg:mt-0 hover:text-white px-4 py-2 rounded hover:bg-blue-700 mr-2">
          <DropdownRender />
        </Link>
      </div>

      <div className="relative mx-auto text-gray-600 lg:block hidden mr-2">
        <input
          className="border-2 border-gray-300 bg-white h-10 pl-2 pr-8 rounded-lg text-sm focus:outline-none"
          type="search"
          name="search"
          placeholder="Search"
        />
        <button
          type="submit"
          className="absolute right-0 top-0 mt-3 mr-2"
        ></button>
      </div>
        <Link
          to="/login"
          className="block text-md px-4 py-2 rounded text-blue-700 font-bold hover:text-white mt-4 hover:bg-blue-700 lg:mt-0"
        >
          Se Connecter
        </Link>
      </div>
  );
  const navSelection = () => {
    if (auth.isAuthenticated) {
      if (auth.user.isSeller) {
        return sellerLinks;
      } else {
        return clientLinks;
      }
    } else {
      return visitorLinks;
    }
  };
  const toggle = () => {
    document.getElementById("nav-content").classList.toggle("hidden");
  };

  return (
    <div>
      <header className="text-gray-700 body-font bg-blue-500  w-full">
        <nav className="flex items-center justify-between flex-wrap bg-white py-4 lg:px-12 shadow border-solid border-t-2 border-blue-700 ">
          <div className="flex justify-between lg:w-auto w-full lg:border-b-0 pl-6 pr-2 border-solid border-b-2 border-gray-300 pb-5 lg:pb-0">
            <div className="flex items-center flex-shrink-0 text-gray-800 mr-16">
              <span className="font-semibold text-xl tracking-tight">
                <Link to= "/" > Het Soumek </Link>
              </span>
            </div>
            {/* responsive header need to be fixed when you are a visitor it dosent work */}
            <div className="block lg:hidden ">
              <button
                id="nav"
                onClick={() => {
                  document
                    .getElementById("nav-content")
                    .classList.toggle("hidden");
                }}
                className="flex items-center px-3 py-2 border-2 rounded text-blue-700 border-blue-700 hover:text-blue-700 hover:border-blue-700"
              >
                <svg
                  className="fill-current h-3 w-3"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>Menu</title>
                  <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                </svg>
              </button>
            </div>
          </div>
          {navSelection()}
        </nav>
      </header>
    </div>
  );
};
Header.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { logout })(Header);
