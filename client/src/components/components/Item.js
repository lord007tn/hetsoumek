import React, { Fragment, useState } from 'react'
import { Redirect, Link, useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import swal from 'sweetalert2'
import Spinner from '../components/Spinner'
import { connect, useSelector } from 'react-redux'
const Item = ({ product }) => {
  const state = useSelector((state) => state)
  const history = useHistory()
  const [time, setTime] = useState('')

  // Set the date we're counting down to
  var countDownDate = new Date(product.endBidTime).getTime()
  //const counter=1;
  // Update the count down every 1 second
  var x = setInterval(function () {
    //counter++;
    // Get today's date and time
    var now = new Date().getTime()

    // Find the distance between now and the count down date
    var distance = countDownDate - now

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24))
    var hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
    )
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    var seconds = Math.floor((distance % (1000 * 60)) / 1000)

    // Display the result in the element with id="demo"
    // document.getElementById('time').innerHTML =
    //   days + 'j ' + hours + 'h ' + minutes + 'm ' + seconds + 's '
    setTime(days + 'j ' + hours + 'h ' + minutes + 'm ' + seconds + 's ')
    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x)
      // document.getElementById('time').innerHTML = 'EXPIRER'
      setTime('EXPIRER')
    }
  }, 1000)
  return (
    <Fragment>
      <div className="lg:w-1/3 md:w-1/2 p-4 w-full bg-gray-200">
        <Link className="block relative h-48 rounded overflow-hidden">
          <img
            alt="ecommerce"
            className="object-cover object-center w-full h-full block"
            src={`http://localhost:8000/uploads/${product.productImage}`}
          />
        </Link>
        <div className="mt-4">
          <h3 className="text-gray-500 text-xs tracking-widest title-font mb-1">
            {product.category.categoryName}
          </h3>
          <h2 className="text-gray-900 title-font text-lg font-medium">
            {product.productName}
          </h2>
          <div className="flex ">
            <div className="flex-initial text-gray-700 text-left  px-5  mt-2">
              <h1 className="mt-1 text-blue-600 font-medium ">
                {' '}
                <span className="font-bold"> Prix actuel:</span>
                {product.lastBidPrice}
              </h1>
              <h2 className="mt-1 text-gray-600  ">
                {' '}
                <span className="font-bold"> Prix de base:</span>
                {product.originalPrice}
              </h2>
            </div>
            <div className="flex-initial text-gray-700 text-center  px-6 ">
              <button
                className="bg-blue-500 hover:bg-blue-700 text-center text-white font-bold py-2 px-6  rounded inline-flex items-center mt-4"
                onClick={() => {
                  if (
                    state.auth != undefined &&
                    state.auth.isAuthenticated == true
                  ) {
                    console.log("auth");
                    history.push(`/${product._id}/details`)
                  } else {
                    swal.fire({
                      title: 'Error!',
                      text: 'Vous devez etre connecter pour continuer',
                      icon: 'error',
                      confirmButtonText: 'Ok',
                    })
                  }
                }}
              >
                s'encherer
              </button>
            </div>
          </div>

          <div className="w-full my-2">
            <div className="text-right">
              <p className=" text-blue-800 font-medium my-2 mx-5" id="time">
                Temps restant: {time}
              </p>
            </div>
            <div className="bg-gray-300 w-full rounded-lg h-2">
              {/* <div
                className="bg-blue-600 rounded-lg h-2"
                style={{ width: lin }}
              ></div> */}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}
Item.prototype = {
  product: PropTypes.object.isRequired,
}

export default Item
