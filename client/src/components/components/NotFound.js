import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import img from '../../assets/img/undraw_page_not_found_su7k.svg'
const NotFound = () => {
  return (
    <Fragment>
    <div className="w-full h-full fixed block top-0 left-0 bg-white opacity-100 z-50">
    <div class="h-screen w-screen bg-white flex justify-center content-center flex-wrap">
        <div className="flex content-center flex-col">
          <img
            src={img}
            style={{ height: '70%', width: '70%', alignSelf: 'center' }}
          />
            <h1 className="text-center m-4 " style={{marginTop:'5rm',fontSize:'25px'}}>désolé, une erreur s'est produite, veuillez réessayer plus tard</h1>
            <Link
            class="mt-4 py-2 px-6 block border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white text-center"
            to="/"
          >
            <i class="fas fa-home"></i>

            <span class="mx-4">Acceuil</span>
          </Link>
        </div>
      
      </div>
    </div>

    </Fragment>
  )
}

export default NotFound
