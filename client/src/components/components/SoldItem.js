import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
const SoldItem = ({product}) => {
    return (
        <Fragment>
            <div className="lg:w-1/3 md:w-1/2 p-4 w-full bg-gray-200">
                <Link className="block relative h-48 rounded overflow-hidden">
                    <img alt="ecommerce" className="object-cover object-center w-full h-full block" src="https://picsum.photos/420/260" />
                </Link>
                <div className="mt-4">
                    <h3 className="text-gray-500 text-xs tracking-widest title-font mb-1">{product.category.categoryName}</h3>
                    <h2 className="text-gray-900 title-font text-lg font-medium">{product.productName}</h2>
                    <div className="flex ">
                        <div className="flex-initial text-gray-700 text-left  px-5  mt-2">
                            <h1 className="mt-1 text-blue-600 font-medium "> <span className="font-bold"> Reached:</span>17 DT </h1>
                            <h2 className="mt-1 text-gray-600  "> <span className="font-bold"> Original price:</span>17 DT </h2>

                        </div>
                    </div>

                    <div className="w-full my-2">
                        {/* <div className="text-right">
                            <p className=" text-blue-800 font-medium my-2 mx-5">Time Left: 00:00:00</p>
                        </div> */}
                        <div className="bg-gray-300 w-full rounded-lg h-2">
                            <div className="bg-blue-600 rounded-lg h-2" style={{ width: "100%" }}></div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
SoldItem.prototype= {
    product: PropTypes.object.isRequired,
}
export default SoldItem