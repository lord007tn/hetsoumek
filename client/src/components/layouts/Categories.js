import React, {useEffect} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {getCategories} from '../../actions/category'
import Spinner from '../components/Spinner'
import CategoryItem from '../components/CategoryItem'
const Categories = ({category, getCategories}) => {
    useEffect(() => {
        getCategories()
    }, [getCategories]);
    return (category.loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :
        <section class="text-gray-700 body-font">
        <div class="container px-5 py-10 mx-auto items-center">
            <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">Catégories</h1>
            <div class="flex flex-wrap mt-5">
            {category.categories && category.categories.map(category => {
                    return <CategoryItem key={category._id} category={category}/>
            })}
            </div>
        </div>
    </section>
    )
}

Categories.propTypes = {
    category: PropTypes.object.isRequired,
    getCategories: PropTypes.func.isRequired,
}
const mapStateToProps = state =>({
    category: state.category
  })
export default connect(mapStateToProps, {getCategories})(Categories)
