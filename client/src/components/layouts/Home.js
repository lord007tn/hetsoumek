import React, { Fragment, useEffect } from 'react'
import img from '../../assets/img/illustration.png'
import Item from '../components/Item'
import SoldItem from '../components/SoldItem'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getProducts, getTodayProducts } from '../../actions/product'
import Spinner from '../components/Spinner'
const Home = ({ auth, product, getProducts, getTodayProducts }) => {
  useEffect(() => {
    getTodayProducts()
    getProducts()
  }, [product.loading])
  return (product.loading ? (
    <div className="disableAllWithSpin">
      <div className="centerSpin">
        <Spinner />
      </div>
    </div>
  ) : (
    <Fragment>
      <section className="text-gray-700 body-font">
        {!auth.isAuthenticated && (
          <Fragment>
            <div className="container flex flex-wrap px-5 py-24 mx-auto items-center">
              <div className="md:w-1/2 md:pr-12 md:py-8 md:border-r md:border-b-0 mb-10 md:mb-0 pb-10 ">
                <h1 className="sm:text-5xl text-2xl font-medium title-font mb-2 text-gray-900">
                  Everything has a value
                </h1>
                <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-gray-900">
                  The best place to bid
                </h1>
                <button className="bg-blue-500 hover:bg-blue-700 text-center text-white font-bold py-2 px-4  rounded inline-flex items-center mt-4">
                  Add goods to sell
                </button>
              </div>
              <div className="flex flex-col md:w-1/2 md:pl-12">
                <img src={img} style={{ height: '80%', width: '80%' }} />
              </div>
            </div>
          </Fragment>
        )}
      </section>
      <section className="text-gray-700 body-font">
        <div className="container px-5 py-2 mx-auto">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">
            Offres se terminant aujourd'hui
          </h1>
          <div className="flex flex-wrap mt-5">
            {product.todayProducts &&
              product.todayProducts.map((product) => {
                return <Item key={product._id} product={product} />
              })}
          </div>
        </div>
      </section>
      <section className="text-gray-700 body-font">
        <div className="container px-5 py-10 mx-auto">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">
            Nouveaux Offres
          </h1>
          <div className="flex flex-wrap mt-5">
          {product.products &&
              product.products.map((product, i) => {
                if(product.state === "0" && i<6){
                  return <Item key={product._id} product={product} />
                }
              })}
          </div>
        </div>
      </section>
      <section className="text-gray-700 body-font">
        <div className="container px-5 py-10 mx-auto">
          <h1 className="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">
            Vendu Récemment
          </h1>
          <div className="flex flex-wrap mt-5">
            {product.products &&
              product.products.map((product, i) => {
                if(product.state === "1" && i<6){
                  return <SoldItem key={product._id} product={product} />
                }
              })}
          </div>
        </div>
      </section>
    </Fragment>
  ))
}
Home.propTypes = {
  auth: PropTypes.object.isRequired,
  product: PropTypes.object.isRequired,
  getProducts: PropTypes.func.isRequired,
  getTodayProducts: PropTypes.func.isRequired,
}
const mapStateToProps = (state) => ({
  auth: state.auth,
  product: state.product,
})
export default connect(mapStateToProps, { getProducts, getTodayProducts })(Home)
