import React, { Fragment, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import Item from '../components/Item'
import { Link } from 'react-router-dom'
import {connect} from 'react-redux';
import Spinner from '../components/Spinner'
import {getProduct} from '../../actions/product'
import {getBids, createBid} from '../../actions/bid'
const DisplayItem = ({ index, bids }) => {
  if (index == 0) {
    return (
      <div>
        <h1>Description</h1>
      </div>
    )
  } else {
    return (
      <div style={{ maxHeight: '250px', overflowY: 'scroll' }}>
        <ul class="flex flex-col  p-4">
          {bids.map((elm) => (
            <li
              class="border-gray-400 flex flex-row mb-2"
              key={Math.random().toString()}
            >
              <div class="select-none cursor-pointer  rounded-md flex flex-1 items-center p-4  transition duration-500 ease-in-out transform hover:-translate-y-1 hover:shadow-lg">
                <div class="flex flex-col rounded-md w-10 h-10 bg-gray-300 justify-center items-center mr-4">
                <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <div class="flex-1 pl-1 mr-16">
                  <div class="font-medium">{elm.name}</div>
                  <div class="text-gray-600 text-sm">Barcha Flouss</div>
                </div>
                <div class="text-gray-600 text-xs">6:00 AM</div>
              </div>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

const ProductDetails = ({auth, product, getProduct, createBid, getBids, match}) => {
  useEffect(() => {
    getProduct(match.params.id)
  }, [product.loading, match.params.id])

  const [selectedIndex, setSelectedIndex] = useState(0)
  const names = ['name1', 'name2', 'name3', 'name4', 'name5']
  const [showBid, setShowbid] = useState(false)
  const [BidData, setBidData] = useState({
    bidID: "",
    bidder:"",
    priceBefore: 0,
    bidMuch:0,
    productID: ""
  });
  const {bidID, bidder, priceBefore, bidMuch, productID} = BidData
  useEffect(() => {
    setBidData({
      ...BidData,
      bidder: auth.loading || !auth.user._id ? '' : auth.user._id,
      priceBefore: product.loading || !product.product.lastBidPrice ? 0: product.product.lastBidPrice,
      productID: product.loading || !product.product._id ? '': product.product._id
    })

  }, [product.loading]);

  const onChange = (e) => {
    setBidData({ ...BidData, [e.target.name]: e.target.value })
    }
  const onSubmit = (e) => {
    e.preventDefault()
    console.log(BidData)
    createBid(BidData, productID)
  }

  return (product.product ===null && product.loading ?
      <Spinner /> :
  <Fragment>
    <section class="text-gray-700 body-font overflow-hidden h-screen">
      <div class="container px-5 py-24 mx-auto">
        <div id="modal" className={` ${showBid ? '' : 'hidden'}`}>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div class="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md">
              <h2 class="text-lg text-gray-700 font-semibold capitalize">
                Ajouter une enchers
              </h2>

              <form onSubmit={(e)=>{
                  onSubmit(e)
                }}>
                <div className="flex flex-wrap">
                  <label class="text-gray-700" for="bidMuch">
                    Montant  à ajouter
                  </label>
                  <input
                    id="bidMuch"
                    name="bidMuch"
                    type="number"
                    placeholder="Montant en TND"
                    value={bidMuch}
                    onChange={(e) => onChange(e)}
                    required
                    class="w-full mt-2 px-4 py-2 block rounded bg-gray-200 text-gray-800 border border-gray-300 focus:outline-none focus:bg-white"
                  />
                </div>
                <div class="flex justify-end mt-4">
                <button class="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600 focus:outline-none focus:bg-red-600 mx-2" onClick={()=>{
                  setShowbid(false)
                }}>
                    Annuler
                  </button>
                  <button type="submit" class="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">
                    Confirmer
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </div>
        <div class="lg:w-4/5 mx-auto flex flex-wrap">
          <div class="lg:w-1/2 w-full lg:pr-10 lg:py-6 mb-6 lg:mb-0">
            <h2 class="text-sm title-font text-gray-500 tracking-widest">
              { product.product.category.categoryName}
            </h2>
            <h1 class="text-gray-900 text-3xl title-font font-medium mb-4">
              {product.product.productName}
            </h1>
            <div class="flex mb-4">
              <a
                href="!#"
                class={`flex-grow border-b-2  py-2 text-lg px-1 ${
                  selectedIndex == 0
                    ? 'text-indigo-500 border-indigo-500'
                    : 'border-gray-300'
                }`}
                onClick={(e) => {
                  e.preventDefault()
                  setSelectedIndex(0)
                }}
              >
                Description
              </a>
              <a
                href="!#"
                class={`flex-grow border-b-2  py-2 text-lg px-1 ${
                  selectedIndex == 1
                    ? 'text-indigo-500 border-indigo-500'
                    : 'border-gray-300'
                }`}
                onClick={(e) => {
                  e.preventDefault()
                  setSelectedIndex(1)
                }}
              >
                Enchers posé
              </a>
              {/* <a href='!#'
                class={`flex-grow border-b-2  py-2 text-lg px-1 ${
                  selectedIndex == 2
                    ? 'text-indigo-500 border-indigo-500'
                    : 'border-gray-300'
                }`}
                onClick={(e) => {
                  e.preventDefault();
                  setSelectedIndex(2)
                }}
              >
                Details
              </a> */}
            </div>
            <p class="leading-relaxed mb-4">
              <DisplayItem index={selectedIndex} bids={names} />
            </p>

            <div class="flex border-t border-gray-300 py-2">
              <span class="text-gray-500">Prix de base</span>
              <span class="ml-auto text-gray-900">{product.product.basePrice}</span>
            </div>
            <div class="flex border-t border-b mb-6 border-gray-300 py-2">
              <span class="text-gray-500">Quantité Disponible</span>
              <span class="ml-auto text-gray-900">{product.product.quantity}</span>
            </div>
            <div class="flex">
              <span class="title-font font-medium text-2xl text-gray-900">
                {product.product.lastBidPrice}
              </span>
              <button
                class="flex ml-auto text-white bg-blue-500 border-0 py-2 px-6 focus:outline-none hover:bg-blue-600 rounded"
                onClick={() => {
                  setShowbid(true)
                }}
              >
                Enchers
              </button>
            </div>
          </div>
          <img
            alt="ecommerce"
            class="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded" style={{height:'400px',width:'400px'}}
            src={`http://localhost:8000/uploads/${product.product.productImage}`}
          />
        </div>
      </div>
      <div class="container px-5 py-10 mx-auto items-center">
        <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">
          Produits dans la meme catégorie
        </h1>
        <div class="flex flex-wrap mt-5">
          {/* <Item />
          <Item />
          <Item />
          <Item />
          <Item />
          <Item /> */}
        </div>
      </div>
    </section>
  </Fragment>
  )
}
ProductDetails.propTypes = {
  product: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  getProduct: PropTypes.func.isRequired,
  getBids: PropTypes.func.isRequired,
  createBid: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  product: state.product,
  auth: state.auth
})
export default connect(mapStateToProps, {getProduct, getBids, createBid})(ProductDetails)
