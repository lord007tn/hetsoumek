import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Moment from 'react-moment'
import Item from '../components/Item';
import {getProducts} from '../../actions/product'
import Spinner from '../components/Spinner'
const ProductsHome = ({getProducts, product}) => {
    useEffect(() => {
        getProducts()
    }, [getProducts]);
    return (product.loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :
        <section class="text-gray-700 body-font h-screen">

            <div class="container px-5 py-10 mx-auto items-center">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-2 text-blue-600 ">New offers</h1>
                <div class="flex flex-wrap mt-5">
                {product.products && product.products.map(product => {
                    if(product.state === "0"){
                        return <Item key={product._id} product={product}/>
                    }
                })}
                </div>
            </div>
        </section>
    )
}
ProductsHome.propTypes = {
    getProducts: PropTypes.func.isRequired,
  }
  
  const mapStateToProps = state =>({
    product: state.product
  })
export default connect(mapStateToProps, {getProducts})(ProductsHome)
