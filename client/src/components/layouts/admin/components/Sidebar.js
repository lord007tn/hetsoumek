import React from "react";

const Sidebar = () => {
  return (
    <div>
      <nav class="mt-10">
        <a
          class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          href="/ui-elements"
        >
          <i class="fas fa-box"></i>

          <span class="mx-4">Produits</span>
        </a>
        <a
          class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          href="/ui-elements"
        >
          <i class="fas fa-user-cog"></i>

          <span class="mx-4">Mon Compte</span>
        </a>
        <a
          class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          href="/ui-elements"
        >
          <i class="fas fa-money-check"></i>

          <span class="mx-4"> Commandes</span>
        </a>
        <a
          class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          href="/ui-elements"
        >
          <i class="fas fa-user-friends"></i>

          <span class="mx-4">Utilisateurs</span>
        </a>
        <a
          class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          href="/ui-elements"
        >
          <i class="fas fa-arrows-alt-h"></i>

          <span class="mx-4">Transactions</span>
        </a>
        <a
          class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          href="/ui-elements"
        >
          <i class="fas fa-gavel"></i>

          <span class="mx-4">Enchers</span>
        </a>
        <a
          class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          href="/ui-elements"
        >
          <i class="fas fa-comment-dots"></i>

          <span class="mx-4">Réclamations</span>
        </a>
      </nav>
    </div>
  );
};

export default Sidebar;
