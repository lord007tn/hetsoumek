import React from "react";
import Swal from "sweetalert2";
//import Swal from 'sweetalert2/dist/sweetalert2.js'
const Tabs = () => {
  const [openTab, setOpenTab] = React.useState(1);
  return (
    <div className="flex flex-wrap">
      <div className="w-full">
        <ul
          className="flex mb-0 list-none flex-wrap pt-3 pb-4 flex-row"
          role="tablist"
        >
          <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
            <a
              className={
                "text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal " +
                (openTab === 1
                  ? "text-white bg-blue-600"
                  : "text-blue-600 bg-white")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(1);
              }}
              data-toggle="tab"
              href="#link1"
              role="tablist"
            >
              Mes Commandes
            </a>
          </li>
          <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
            <a
              className={
                "text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal " +
                (openTab === 2
                  ? "text-white bg-blue-600"
                  : "text-blue-600 bg-white")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(2);
              }}
              data-toggle="tab"
              href="#link2"
              role="tablist"
            >
              Historique
            </a>
          </li>
          <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
            <a
              className={
                "text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal " +
                (openTab === 3
                  ? "text-white bg-blue-600"
                  : "text-blue-600 bg-white")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(3);
              }}
              data-toggle="tab"
              href="#link3"
              role="tablist"
            >
              Notifications
            </a>
          </li>
          <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
            <a
              className={
                "text-xs font-bold uppercase px-5 py-3 shadow-lg rounded block leading-normal " +
                (openTab === 4
                  ? "text-white bg-blue-600"
                  : "text-blue-600 bg-white")
              }
              onClick={(e) => {
                e.preventDefault();
                setOpenTab(4);
              }}
              data-toggle="tab"
              href="#link4"
              role="tablist"
            >
              My Wallet
            </a>
          </li>
        </ul>
        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
          <div className="px-4 py-5 flex-auto">
            <div className="tab-content tab-space w-full">
              <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                <table class="table-auto my-2 w-full">
                  <thead className="bg-blue-500">
                    <tr>
                      <th class="px-4 py-2 text-white">Date</th>
                      <th class="px-4 py-2 text-white">Produit</th>
                      <th class="px-4 py-2 text-white">Prix</th>
                      <th class="px-4 py-2 text-white">Type</th>
                      <th class="px-4 py-2 text-white">Status</th>
                      <th class="px-4 py-2 text-white">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                <table class="table-auto my-2 w-full">
                  <thead className="bg-blue-500">
                    <tr>
                      <th class="px-4 py-2 text-white">Date & Heure</th>
                      <th class="px-4 py-2 text-white">Actions Réalisé</th>
                      <th class="px-4 py-2 text-white">Description</th>
                      <th class="px-4 py-2 text-white">Status</th>
                      <th class="px-4 py-2 text-white">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className={openTab === 3 ? "block" : "hidden"} id="link3">
                <table class="table-auto my-2 w-full">
                  <thead className="bg-blue-500">
                    <tr>
                      <th class="px-4 py-2 text-white">Date</th>
                      <th class="px-4 py-2 text-white">Produit</th>
                      <th class="px-4 py-2 text-white">Prix</th>
                      <th class="px-4 py-2 text-white">Type</th>
                      <th class="px-4 py-2 text-white">Status</th>
                      <th class="px-4 py-2 text-white">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className={openTab === 4 ? "block" : "hidden"} id="link4">
                <button
                  class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded flex-end"
                  onClick={async () => {
                    const { value: ipAddress } = await Swal.fire({
                      title: "Ajouter Votre Code Coupon",
                      input: "text",
                      inputValue: "",
                      showCancelButton: true,
                      inputValidator: (value) => {
                        if (!value) {
                          return "You need to write something!";
                        }
                      },
                    });
                  }}
                >
                  <i class="far fa-credit-card pr-5"></i>
                  Recharger
                </button>
                <table class="table-auto my-2 w-full">
                  <thead className="bg-blue-500">
                    <tr>
                      <th class="px-4 py-2 text-white">Date & Heure</th>
                      <th class="px-4 py-2 text-white">Type</th>
                      <th class="px-4 py-2 text-white">Description</th>
                      <th class="px-4 py-2 text-white">Status</th>
                      <th class="px-4 py-2 text-white">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                    <tr>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">858</td>
                      <td class="border px-4 py-2">Intro to CSS</td>
                      <td class="border px-4 py-2">Adam</td>
                      <td class="border px-4 py-2">858</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tabs;
