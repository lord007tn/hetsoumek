import React, { useEffect } from 'react'
import {connect} from 'react-redux';
import PropTypes from 'prop-types'
import {getBid, getBids} from '../../../../actions/bid'
const Bids = ({bid, getBid, getBids}) => {
  useEffect(() => {
    getBids()
  }, [bid.loading]);
  return (
    <div class="mb-2 -mx-3">
    <div class="w-full p-3">
      <div class="bg-white rounded-md shadow">
        <div class="px-5 py-4 font-bold bg-gray-100 border-b text-theme-color text-md rounded-t-md flex">
          {/* <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded content-end">
            Ajouter
          </button> */}
        </div>
        <div class="pt-3 text-gray-700 text-md">
          <table class="w-full text-left">
            <thead>
              <tr class="bg-gray-50">
                <th class="px-4 py-2">Name</th>
                <th class="px-4 py-2">Position</th>
                <th class="px-4 py-2">Office</th>
                <th class="px-4 py-2">Age</th>
                <th class="px-4 py-2">Start Date</th>
              </tr>
            </thead>
            <tbody>
            {bid.bids && bid.bids.map((bid)=>{
              return(
                <tr>
                <td class="px-4 py-2">{bid._id}</td>
                <td class="px-4 py-2">{bid.bidder.firstName} {bid.bidder.lastName}</td>
                <td class="px-4 py-2">Singapore</td>
                <td class="px-4 py-2">28</td>
                <td class="px-4 py-2">2010/11/14</td>
              </tr>
              )
            })}

            </tbody>
          </table>
          <div class="py-2 m-2 text-right">
            <span class="relative inline-flex text-gray-600 -z-1">
              <button
                type="button"
                class="relative inline-flex items-center px-2 py-2 text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-l-md hover:text-gray-500 focus:outline-none "
              >
                <svg
                  fill="none"
                  stroke="currentColor"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  viewBox="0 0 24 24"
                  class="w-4 h-4"
                >
                  <path d="M15 19l-7-7 7-7"></path>
                </svg>
              </button>
              <button
                type="button"
                class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
              >
                1
              </button>
              <button
                type="button"
                class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
              >
                2
              </button>
              <button
                type="button"
                class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
              >
                4
              </button>
              <button
                type="button"
                class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
              >
                5
              </button>
              <button
                type="button"
                class="relative inline-flex items-center px-2 py-2 -ml-px text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-r-md hover:text-gray-500 focus:outline-none "
              >
                <svg
                  fill="none"
                  stroke="currentColor"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  viewBox="0 0 24 24"
                  class="w-4 h-4"
                >
                  <path d="M9 5l7 7-7 7"></path>
                </svg>
              </button>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  );
};
Bids.propTypes = {
  bid: PropTypes.object.isRequired,
  getBid: PropTypes.func.isRequired,
  getBids: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  bid: state.bid
})
export default connect(mapStateToProps, {getBid, getBids})(Bids);
