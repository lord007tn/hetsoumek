import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Moment from 'react-moment'
import {getCategories, getCategory, createCategory, updateCategory, deleteCategory} from '../../../../actions/category'
const Categories = ({category, getCategories, getCategory, createCategory, updateCategory, deleteCategory}) => {
  const [AddModalVisible, setAddModalVisible] = useState(false)
  const [EditModalVisible, setEditModalVisible] = useState(false)
  const [CategoryData, setCategoryData] = useState({
    categoryID: "",
    categoryName: "",
    about:""
  })
  const {categoryID, categoryName, about} = CategoryData
  useEffect(() => {
    getCategories()
  }, [category.loading]);
  const onChange = e =>{
    setCategoryData({...CategoryData, [e.target.name]: e.target.value})
}
const onSubmit = e =>{
  e.preventDefault()
  createCategory(CategoryData);
  setCategoryData({
    categoryID:"",
    categoryName: "",
    about:""
  })
  getCategories();
  setAddModalVisible(false);
}
const onEditClick = (e, index) => {
  const categoryEdit = category.categories[index];
  console.log(categoryEdit)
  setCategoryData({
    categoryID: category.loading || !categoryEdit._id ? "" : categoryEdit._id,
    categoryName: category.loading || !categoryEdit.categoryName ? "" : categoryEdit.categoryName,
    about: category.loading || !categoryEdit.about ? "" : categoryEdit.about
  })
  setEditModalVisible(true)
}
const onEditSubmit = (e) => {
  e.preventDefault();
  updateCategory(CategoryData, categoryID);
  setCategoryData({
    categoryName: "",
    about:""
  })
  getCategories();
  setEditModalVisible(false);
}
  return (
    <div class="mb-2 -mx-3">
      <div class="w-full p-3">
        <div class="bg-white rounded-md shadow">
          <div class="px-5 py-4 font-bold bg-gray-100 border-b text-theme-color text-md rounded-t-md flex">
            <button
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded content-end float-right"
              onClick={() => {
                setAddModalVisible(true);
              }}
            >
              Ajouter Catégorie
            </button>
          </div>
          <div id="modal" className={` ${AddModalVisible ? '' : 'hidden'}`}>
            <div
              className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                    <h3 className="text-3xl font-semibold">Ajouter une catégorie</h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setAddModalVisible(false)}
                    >
                      <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative p-6 flex-auto">
                  <form className="px-8 pt-6 pb-8 mb-4 bg-white rounded" onSubmit={e => onSubmit(e)}>
                                <div className="mb-4">
                                <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="categoryName">
                                    Nom de catégorie
                                </label>
                                <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    id="categoryName"
                                    name="categoryName"
                                    type="text"
                                    placeholder="Nom de catégorie"
                                    value={categoryName}
                                    onChange={e => onChange(e)}
                                    required
                                    />
                                </div>
                                <div className="mb-4">
                                <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="about">
                                    Description
                                </label>
                                <textarea className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    id="about"
                                    name="about"
                                    type="text"
                                    placeholder="Description"
                                    value={about}
                                    onChange={e => onChange(e)}
                                    required
                                    />
                                </div>
                  <div className="flex items-center justify-end pt-6 border-solid border-gray-300 rounded-b">
                    <button
                      className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                      type="button"
                      style={{ transition: 'all .15s ease' }}
                      onClick={() => setAddModalVisible(false)}
                    >
                      Fermer
                    </button>
                    <button
                      className="bg-green-500 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                      type="submit"
                      style={{ transition: 'all .15s ease' }}
                    >
                      Ajouter
                    </button>
                  </div>
                            </form>
                  </div>
                  {/*footer*/}

                </div>
              </div>
            </div>
          </div>
          <div id="modalEdit" className={` ${EditModalVisible ? '' : 'hidden'}`}>
            <div
              className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                    <h3 className="text-3xl font-semibold">Ajouter une catégorie</h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setEditModalVisible(false)}
                    >
                      <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  {/*body*/}
                  <div className="relative p-6 flex-auto">
                  <form className="px-8 pt-6 pb-8 mb-4 bg-white rounded" onSubmit={e => onEditSubmit(e)}>
                                <div className="mb-4">
                                <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="categoryName">
                                    Nom de catégorie
                                </label>
                                <input className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    id="categoryName"
                                    name="categoryName"
                                    type="text"
                                    placeholder="Nom de catégorie"
                                    value={categoryName}
                                    onChange={e => onChange(e)}
                                    required
                                    />
                                </div>
                                <div className="mb-4">
                                <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="about">
                                    Description
                                </label>
                                <textarea className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                                    id="about"
                                    name="about"
                                    type="text"
                                    placeholder="Description"
                                    value={about}
                                    onChange={e => onChange(e)}
                                    required
                                    />
                                </div>
                  <div className="flex items-center justify-end pt-6 border-solid border-gray-300 rounded-b">
                    <button
                      className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                      type="button"
                      style={{ transition: 'all .15s ease' }}
                      onClick={() => setEditModalVisible(false)}
                    >
                      Fermer
                    </button>
                    <button
                      className="bg-green-500 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                      type="submit"
                      style={{ transition: 'all .15s ease' }}
                    >
                      Modifier
                    </button>
                  </div>
                            </form>
                  </div>
                  {/*footer*/}

                </div>
              </div>
            </div>
          </div>
          <div class="pt-3 text-gray-700 text-md">
            <table class="w-full text-left">
              <thead>
                <tr class="bg-gray-50">
                  <th class="px-4 py-2">Catégorie</th>
                  <th class="px-4 py-2">Description</th>
                  <th class="px-4 py-2">Date de creation</th>
                  <th class="px-4 py-2">Actions</th>
                </tr>
              </thead>
              <tbody>
              {category.categories && category.categories.map((category, i) => {
                return(
                  <tr>
                  <td class="px-4 py-2">{category.categoryName}</td>
                  <td class="px-4 py-2">{category.about}</td>
                  <td class="px-4 py-2"><Moment format='MMMM Do YYYY hh:mm:ss'>{category.createdAt}</Moment></td>
                  <td class="px-4 py-2">
                    <button class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white" onClick={(e) => onEditClick(e, i)}>
                      <i class="fas fa-edit"></i>
                    </button>
                    <button class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white" onClick={() => {
                      deleteCategory(category._id);
                      getCategories();
                    }}>
                      <i class="fas fa-trash"></i>
                    </button>
                  </td>
                </tr>
                )
              })}

              </tbody>
            </table>
            <div class="py-2 m-2 text-right">
              <span class="relative inline-flex text-gray-600 -z-1">
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-l-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M15 19l-7-7 7-7"></path>
                  </svg>
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  1
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  2
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  4
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  5
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 -ml-px text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-r-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M9 5l7 7-7 7"></path>
                  </svg>
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>

  )
}
Categories.propTypes = {
  category: PropTypes.object.isRequired,
  getCategories: PropTypes.func.isRequired,
  getCategory: PropTypes.func.isRequired,
  createCategory: PropTypes.func.isRequired,
  updateCategory: PropTypes.func.isRequired,
  deleteCategory: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  category: state.category
})
export default connect(mapStateToProps, {getCategories, getCategory, createCategory, updateCategory, deleteCategory})(Categories)
