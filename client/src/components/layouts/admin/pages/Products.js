import React, { useState, useEffect } from 'react'
import ImageUploader from 'react-images-upload'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Spinner from '../../../components/Spinner'
import Swal from 'sweetalert2'
import Moment from 'react-moment'
import moment from 'moment'
import {
  updateProduct,
  getProduct,
  getProducts,
  createProduct,
  deleteProduct,
} from '../../../../actions/product'
import { getCategories } from '../../../../actions/category'
const Products = ({
  auth: { loading, user },
  product,
  category,
  getCategories,
  getProduct,
  getProducts,
  updateProduct,
  deleteProduct,
  createProduct,
}) => {
  useEffect(() => {
    getProducts()
  }, [product.loading])
  const [AddModalVisible, setAddModalVisible] = useState(false)
  const [EditModalVisible, setEditModalVisible] = useState(false)
  const [Upload, setUpload] = useState({ image: '' })
  const [ProductData, setProductData] = useState({
    productID: '',
    productName: '',
    imageName: '',
    description: '',
    originalPrice: '',
    basePrice: '',
    quantity: '',
    categoryID: '',
    seller: '',
    startBidTime: '',
    endBidTime: '',
    bidAmount: '',
  })
  const {
    productID,
    productName,
    description,
    originalPrice,
    basePrice,
    quantity,
    seller,
    startBidTime,
    endBidTime,
    bidAmount,
    categoryID,
  } = ProductData

  const [Categories, setCategories] = useState([])

  useEffect(() => {
    getCategories()
    setCategories(
      category.loading || !category.categories ? [] : category.categories,
    )
  }, [category.loading])
  useEffect(() => {
    setProductData({
      seller: loading || !user._id ? '' : user._id,
    })
  }, [loading])
  const onChange = (e) => {
    setProductData({ ...ProductData, [e.target.name]: e.target.value })
  }
  const onChangeImage = (e) => {
    setUpload({
      image: e[0],
    })
  }
  const onChangeCategory = (e, i) => {
    const { name, value } = e.target
    if (ProductData.category !== '') {
      setProductData({ ...ProductData, categoryID: value })
    }
  }
  const onSubmit = (e) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('productImage', Upload.image)
    formData.append('productName', productName)
    formData.append('description', description)
    formData.append('originalPrice', originalPrice)
    formData.append('basePrice', basePrice)
    formData.append('quantity', quantity)
    formData.append('seller', seller)
    formData.append('category', categoryID)
    formData.append('startBidTime', startBidTime)
    formData.append('endBidTime', endBidTime)
    formData.append('bidAmount', bidAmount)
    createProduct(formData)
    getProducts()
    setProductData({
      productID: '',
      productName: '',
      imageName: '',
      description: '',
      originalPrice: '',
      basePrice: '',
      quantity: '',
      categoryID: '',
      startBidTime: '',
      endBidTime: '',
      bidAmount: '',
    })
    setAddModalVisible(false)
  }

  const onEditClick = (e, index) => {
    const productEdit = product.products[index]
    setProductData({
      productID: product.loading || !productEdit._id ? '' : productEdit._id,
      productName:
        product.loading || !productEdit.productName
          ? ''
          : productEdit.productName,
      description:
        product.loading || !productEdit.description
          ? ''
          : productEdit.description,
      originalPrice:
        product.loading || !productEdit.originalPrice
          ? ''
          : productEdit.originalPrice,
      basePrice:
        product.loading || !productEdit.basePrice ? '' : productEdit.basePrice,
      quantity:
        product.loading || !productEdit.quantity ? '' : productEdit.quantity,
      categoryID:
        product.loading || !productEdit.category._id
          ? ''
          : productEdit.category._id,
      seller:
        product.loading || !productEdit.seller._id
          ? ''
          : productEdit.seller._id,
      startBidTime:
        product.loading || !productEdit.startBidTime
          ? ''
          : productEdit.startBidTime,
      endBidTime:
        product.loading || !productEdit.endBidTime
          ? ''
          : productEdit.endBidTime,
      bidAmount:
        product.loading || !productEdit.bidAmount ? '' : productEdit.bidAmount,
    })
    setEditModalVisible(true)
  }

  const onEditSubmit = (e) => {
    e.preventDefault()
    const formData = new FormData()
    formData.append('productImage', Upload.image)
    formData.append('productName', productName)
    formData.append('description', description)
    formData.append('originalPrice', originalPrice)
    formData.append('basePrice', basePrice)
    formData.append('quantity', quantity)
    formData.append('seller', seller)
    formData.append('category', categoryID)
    formData.append('startBidTime', startBidTime)
    formData.append('endBidTime', endBidTime)
    formData.append('bidAmount', bidAmount)
    console.log(ProductData)
    updateProduct(productID, formData)
    setProductData({
      productID: '',
      productName: '',
      imageName: '',
      description: '',
      originalPrice: '',
      basePrice: '',
      quantity: '',
      categoryID: '',
      startBidTime: '',
      endBidTime: '',
      bidAmount: '',
    })
    getProducts()
    setEditModalVisible(false)
  }
  return (
    <div class="mb-2 -mx-3">
      <div class="w-full p-3">
        <div class="bg-white rounded-md shadow">
          <div class="px-5 py-4 font-bold bg-gray-100 border-b text-theme-color text-md rounded-t-md flex">
            <button
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded content-end float-right"
              onClick={(e) => {
                setAddModalVisible(true)
                e.preventDefault()
              }}
            >
              Ajouter Produit
            </button>
          </div>

          <div
            id="productAddModal"
            className={` ${AddModalVisible ? '' : 'hidden'}`}
          >
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                    <h3 className="text-3xl font-semibold">
                      Ajouter un produit
                    </h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setAddModalVisible(false)}
                    >
                      <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  <div className="relative p-6 flex-auto">
                    <form
                      className="px-8 pt-6 pb-8 mb-4 bg-white rounded"
                      onSubmit={(e) => onSubmit(e)}
                    >
                      <div className="mb-4">
                        <ImageUploader
                          className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                          buttonClassName="bg-gray-700 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                          withPreview={true}
                          withIcon={true}
                          buttonText="choisir une image"
                          onChange={(e) => onChangeImage(e)}
                          imgExtension={['.jpg', '.gif', '.png', '.gif']}
                          label="Max file size: 5mb, accepted: jpg, gif, png"
                          maxFileSize={5242880}
                          singleImage={true}
                        />
                      </div>
                      <div className="mb-4">
                        <label
                          className="block tracking-wide text-gray-700 text-xs font-bold mb-2"
                          htmlFor="productName"
                        >
                          Nom de produit
                        </label>
                        <input
                          className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                          id="productName"
                          name="productName"
                          type="text"
                          placeholder="Nom de produit"
                          value={productName}
                          onChange={(e) => onChange(e)}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <label
                          className="block tracking-wide text-gray-700 text-xs font-bold mb-2"
                          htmlFor="description"
                        >
                          Description
                        </label>
                        <textarea
                          className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                          id="description"
                          name="description"
                          type="text"
                          placeholder="Description"
                          value={description}
                          onChange={(e) => onChange(e)}
                          required
                        />
                      </div>
                      <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="originalPrice"
                          >
                            Prix d'origin
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="originalPrice"
                            min="0"
                            type="number"
                            name="originalPrice"
                            placeholder="TND"
                            value={originalPrice}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/2 px-3">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="basePrice"
                          >
                            Prix initial
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="basePrice"
                            min="0"
                            type="number"
                            name="basePrice"
                            placeholder="TND"
                            value={basePrice}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                      </div>
                      <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="quantity"
                          >
                            Quantité
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="quantity"
                            type="number"
                            min="0"
                            name="quantity"
                            placeholder="Quantité"
                            value={quantity}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="bidAmount"
                          >
                            Taux de bid
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="bidAmount"
                            type="number"
                            min="0"
                            name="bidAmount"
                            placeholder="Taux de bid"
                            value={bidAmount}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/3 px-3">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="category"
                          >
                            Categorie
                          </label>
                          <select
                            onChange={(e) => onChangeCategory(e)}
                            name="category"
                            placeholder=""
                            class="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="category"
                          >
                            <option value="">---</option>
                            {Categories.map((x, i) => {
                              return (
                                <option key={i} value={x._id}>
                                  {x.categoryName}
                                </option>
                              )
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="startBidTime"
                          >
                            Date debut
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="startBidTime"
                            type="datetime-local"
                            name="startBidTime"
                            value={startBidTime}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/2 px-3">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="endBidTime"
                          >
                            Date fin
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="endBidTime"
                            type="datetime-local"
                            name="endBidTime"
                            value={endBidTime}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                      </div>
                      <div className="flex items-center justify-end pt-6 border-solid border-gray-300 rounded-b">
                        <button
                          className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                          style={{ transition: 'all .15s ease' }}
                          onClick={() => setAddModalVisible(false)}
                        >
                          Fermer
                        </button>
                        <button
                          className="bg-green-500 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                          type="submit"
                          style={{ transition: 'all .15s ease' }}
                        >
                          Ajouter
                        </button>
                      </div>
                    </form>
                  </div>
                  {/*footer*/}
                </div>
              </div>
            </div>
          </div>
          <div
            id="productEditModal"
            className={` ${EditModalVisible ? '' : 'hidden'}`}
          >
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                {/*content*/}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                  {/*header*/}
                  <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t">
                    <h3 className="text-3xl font-semibold">
                      Ajouter un produit
                    </h3>
                    <button
                      className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                      onClick={() => setEditModalVisible(false)}
                    >
                      <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                        ×
                      </span>
                    </button>
                  </div>
                  <div className="relative p-6 flex-auto">
                    <form
                      className="px-8 pt-6 pb-8 mb-4 bg-white rounded"
                      onSubmit={(e) => onEditSubmit(e)}
                    >
                      <div className="mb-4">
                        <ImageUploader
                          className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                          buttonClassName="bg-gray-700 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                          withPreview={true}
                          withIcon={true}
                          buttonText="choisir une image"
                          onChange={(e) => onChangeImage(e)}
                          imgExtension={['.jpg', '.gif', '.png', '.gif']}
                          label="Max file size: 5mb, accepted: jpg, gif, png"
                          maxFileSize={5242880}
                          singleImage={true}
                        />
                      </div>
                      <div className="mb-4">
                        <label
                          className="block tracking-wide text-gray-700 text-xs font-bold mb-2"
                          htmlFor="productName"
                        >
                          Nom de produit
                        </label>
                        <input
                          className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                          id="productName"
                          name="productName"
                          type="text"
                          placeholder="Nom de produit"
                          value={productName}
                          onChange={(e) => onChange(e)}
                          required
                        />
                      </div>
                      <div className="mb-4">
                        <label
                          className="block tracking-wide text-gray-700 text-xs font-bold mb-2"
                          htmlFor="description"
                        >
                          Description
                        </label>
                        <textarea
                          className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                          id="description"
                          name="description"
                          type="text"
                          placeholder="Description"
                          value={description}
                          onChange={(e) => onChange(e)}
                          required
                        />
                      </div>
                      <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="originalPrice"
                          >
                            Prix d'origin
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="originalPrice"
                            min="0"
                            type="number"
                            name="originalPrice"
                            placeholder="TND"
                            value={originalPrice}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/2 px-3">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="basePrice"
                          >
                            Prix initial
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="basePrice"
                            min="0"
                            type="number"
                            name="basePrice"
                            placeholder="TND"
                            value={basePrice}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                      </div>
                      <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="quantity"
                          >
                            Quantité
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="quantity"
                            type="number"
                            min="0"
                            name="quantity"
                            placeholder="Quantité"
                            value={quantity}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="bidAmount"
                          >
                            Taux de bid
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="bidAmount"
                            type="number"
                            min="0"
                            name="bidAmount"
                            placeholder="Taux de bid"
                            value={bidAmount}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/3 px-3">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="category"
                          >
                            Categorie
                          </label>
                          <select
                            onChange={(e) => onChangeCategory(e)}
                            name="category"
                            placeholder=""
                            class="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="category"
                          >
                            {Categories.map((x, i) => {
                              if (x._id === categoryID) {
                                return (
                                  <option key={i} value={x._id} selected>
                                    {x.categoryName}
                                  </option>
                                )
                              } else {
                                return (
                                  <option key={i} value={x._id}>
                                    {x.categoryName}
                                  </option>
                                )
                              }
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="startBidTime"
                          >
                            Date debut
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="startBidTime"
                            type="datetime-local"
                            name="startBidTime"
                            value={moment(startBidTime).format(
                              'YYYY-MM-DDTHH:mm:ss',
                            )}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                        <div className="w-full md:w-1/2 px-3">
                          <label
                            className="block mb-2 text-sm font-bold text-gray-700"
                            htmlFor="endBidTime"
                          >
                            Date fin
                          </label>
                          <input
                            className="w-full px-3 py-2 text-sm leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                            id="endBidTime"
                            type="datetime-local"
                            name="endBidTime"
                            value={moment(endBidTime).format(
                              'YYYY-MM-DDTHH:mm:ss',
                            )}
                            onChange={(e) => onChange(e)}
                            required
                          />
                        </div>
                      </div>
                      <div className="flex items-center justify-end pt-6 border-solid border-gray-300 rounded-b">
                        <button
                          className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1"
                          type="button"
                          style={{ transition: 'all .15s ease' }}
                          onClick={() => setEditModalVisible(false)}
                        >
                          Fermer
                        </button>
                        <button
                          className="bg-green-500 text-white active:bg-green-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
                          type="submit"
                          style={{ transition: 'all .15s ease' }}
                        >
                          Modifier
                        </button>
                      </div>
                    </form>
                  </div>
                  {/*footer*/}
                </div>
              </div>
            </div>
          </div>
          <div class="pt-3 text-gray-700 text-md">
            <table class="w-full text-left">
              <thead>
                <tr class="bg-gray-50">
                  <th class="px-4 py-2">Nom de produit</th>
                  <th class="px-4 py-2">Image</th>
                  <th class="px-4 py-2">Prix initial</th>
                  <th class="px-4 py-2">prix de base</th>
                  <th class="px-4 py-2">Date de creation</th>
                  <th class="px-4 py-2">Actions</th>
                </tr>
              </thead>
              <tbody>
                {product.products &&
                  product.products.map((product, i) => {
                    console.log('My prodcut', product)
                    return (
                      <tr>
                        <td class="px-4 py-2">{product.productName}</td>
                        <td class="px-4 py-2">
                          <img
                            src={`http://localhost:8000/uploads/${product.productImage}`}
                            height="100px"
                            width="100px"
                          />
                        </td>
                        <td class="px-4 py-2">{product.basePrice}</td>
                        <td class="px-4 py-2">{product.basePrice}</td>
                        <td class="px-4 py-2">
                          <Moment format="MMMM Do YYYY hh:mm:ss">
                            {product.createdAt}
                          </Moment>
                        </td>
                        <td class="px-4 py-2">
                          <button
                            class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white"
                            onClick={(e) => onEditClick(e, i)}
                          >
                            <i class="fas fa-edit"></i>
                          </button>
                          <button
                            class="bg-blue-500 cursor-pointer rounded p-1 mx-1 text-white"
                            onClick={() => {
                              Swal.fire({
                                title: 'Supression Produit',
                                text: 'Voulez vous confirmer la supression ?',
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Oui',
                                cancelButtonText: 'Non',
                                reverseButtons: true,
                              }).then((result) => {
                                if (result.value) {
                                  deleteProduct(product._id)
                                  getProducts()
                                } else if (
                                  /* Read more about handling dismissals below */
                                  result.dismiss === Swal.DismissReason.cancel
                                ) {
                                }
                              })
                            }}
                          >
                            <i class="fas fa-trash"></i>
                          </button>
                        </td>
                      </tr>
                    )
                  })}
              </tbody>
            </table>
            <div class="py-2 m-2 text-right">
              <span class="relative inline-flex text-gray-600 -z-1">
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-l-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M15 19l-7-7 7-7"></path>
                  </svg>
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  1
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 hover:text-gray-500 focus:outline-none "
                >
                  2
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  4
                </button>
                <button
                  type="button"
                  class="relative items-center hidden px-4 py-2 -ml-px text-sm leading-5 bg-white border border-gray-300 md:inline-flex hover:text-gray-500 focus:outline-none "
                >
                  5
                </button>
                <button
                  type="button"
                  class="relative inline-flex items-center px-2 py-2 -ml-px text-sm leading-5 text-gray-500 bg-white border border-gray-300 rounded-r-md hover:text-gray-500 focus:outline-none "
                >
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    viewBox="0 0 24 24"
                    class="w-4 h-4"
                  >
                    <path d="M9 5l7 7-7 7"></path>
                  </svg>
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
Products.propTypes = {
  auth: PropTypes.object.isRequired,
  product: PropTypes.object.isRequired,
  category: PropTypes.object.isRequired,
  createProduct: PropTypes.func.isRequired,
  getProduct: PropTypes.func.isRequired,
  getProducts: PropTypes.func.isRequired,
  updateProduct: PropTypes.func.isRequired,
  deleteProduct: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  product: state.product,
  category: state.category,
})
export default connect(mapStateToProps, {
  createProduct,
  getProducts,
  getProduct,
  updateProduct,
  deleteProduct,
  getCategories,
})(Products)
