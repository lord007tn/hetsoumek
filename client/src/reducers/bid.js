import {
    BID_ERROR,
    GET_BID,
    GET_BIDS,
    CREATE_BID
} from '../actions/types';

const initialState = {
    bids: [],
    bid: null,
    loading: true,
    error: {}
}
export default function(state = initialState, action) {
    const {type, payload} = action;

    switch (type) {
        case GET_BIDS:
            return{
                ...state,
                bids: payload,
                loading: false
            }
        case GET_BID:
            return{
                ...state,
                bid: payload,
                loading: false
            }
        case CREATE_BID:
            return{
                ...state,
                loading: false
            }
        case BID_ERROR:
            return{
                ...state,
                error: payload,
                loading: false
            }
        default:
            return state
    }
}