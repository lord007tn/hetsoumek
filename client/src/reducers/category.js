import {
    CATEGORY_ERROR,
    GET_CATEGORIES,
    GET_CATEGORY,
    DELETE_CATEGORY,
    UPDATE_CATEGORY,
    CREATE_CATEGORY
} from '../actions/types';

const initialState = {
    categories: [],
    category: null,
    loading: true,
    error: {}
}
export default function(state = initialState, action) {
    const {type, payload} = action;

    switch (type) {
        case GET_CATEGORIES:
            return{
                ...state,
                categories: payload,
                loading: false
            }
        case GET_CATEGORY:
            return{
                ...state,
                category: payload,
                loading: false
            }
        case CREATE_CATEGORY:
            return{
                ...state,
                loading: false
            }
        case UPDATE_CATEGORY:
            return{
                ...state,
                loading: false
            }
        case DELETE_CATEGORY:
            return{
                ...state,
                stories: state.categories.filter(category => category._id !== payload),
                loading: false
            }
        case CATEGORY_ERROR:
            return{
                ...state,
                error: payload,
                loading: false
            }
        default:
            return state
    }
}