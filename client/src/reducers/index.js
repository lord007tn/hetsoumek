import {combineReducers} from 'redux';
import auth from './auth'
import category from './category'
import product from './product'
import user from './user'
import bid from './bid';
export default combineReducers({
    auth,
    category,
    product,
    user,
    bid
})