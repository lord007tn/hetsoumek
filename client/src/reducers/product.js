import {
    PRODUCT_ERROR,
    GET_PRODUCTS,
    GET_PRODUCT,
    CREATE_PRODUCT,
    GET_TODAY_PRODUCTS,
    UPDATE_PRODUCT,
    DELETE_PRODUCT
} from '../actions/types';

const initialState = {
    products: [],
    todayProducts: [],
    product: null,
    loading: true,
    error: {}
}
export default function(state = initialState, action) {
    const {type, payload} = action;

    switch (type) {
        case GET_PRODUCTS:
            return{
                ...state,
                products: payload,
                loading: false
            }
        case GET_TODAY_PRODUCTS:
            return{
                ...state,
                todayProducts: payload,
                loading: false
            }
        case GET_PRODUCT:
            return{
                ...state,
                product: payload,
                loading: false
            }
        case PRODUCT_ERROR:
            return{
                ...state,
                error: payload,
                loading: false
            }
        case CREATE_PRODUCT:
            return{
                ...state,
                loading: false
            }
        case UPDATE_PRODUCT:
            return{
                ...state,
                loading: false
            }
        case DELETE_PRODUCT:
            return{
                ...state,
                stories: state.products.filter(product => product._id !== payload),
                loading: false
            }
        default:
            return state
    }
}