import React from 'react'
import { Route, Switch } from 'react-router-dom'
import AdminRoute from './AdminRoute'
import NotFound from '../components/components/NotFound';
import Profile from '../components/layouts/Profile'
import Products from '../components/layouts/admin/pages/Products'
import Categories from '../components/layouts/admin/pages/Categories'
import { BrowserRouter as Router, useRouteMatch, Link } from 'react-router-dom'
import Transactions from '../components/layouts/admin/pages/Transactions'
import Settings from '../components/layouts/admin/pages/Settings'
import Feedback from '../components/layouts/admin/pages/Feedback'
import Bids from '../components/layouts/admin/pages/Bids'
import Commandes from '../components/layouts/admin/pages/Commandes'
import Users from '../components/layouts/admin/pages/Users'
const AdminDashboardRoutes = (props) => {
  let match = useRouteMatch()
  return (
    <div class="flex">
      <div class="fixed z-20 inset-0 bg-white opacity-50 transition-opacity shadow border-solid  lg:hidden h-screen"></div>
      <div class="fixed z-30 inset-y-0 left-0 w-64 transition duration-300 transform bg-white overflow-y-auto lg:translate-x-0 lg:static lg:inset-0 border-r-2 border-blue-700 h-screen">
        <nav class="mt-10">
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to="/admin/products"
          >
            <i class="fas fa-box"></i>

            <span class="mx-4">Produits</span>
          </Link>
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to="/admin/categories"
          >
            <i class="fas fa-th"></i>
            <span class="mx-4">Categories</span>
          </Link>
          <Link
            to="/admin/settings"
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          >
            <i class="fas fa-user-cog"></i>

            <span class="mx-4">Mon Compte</span>
          </Link>
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to="/admin/commandes"
          >
            <i class="fas fa-money-check"></i>

            <span class="mx-4"> Commandes</span>
          </Link>

          <Link
            to="/admin/users"
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
          >
            <i class="fas fa-user-friends"></i>

            <span class="mx-4">Utilisateurs</span>
          </Link>

          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to="/admin/bids"
          >
            <i class="fas fa-gavel"></i>

            <span class="mx-4">Enchers</span>
          </Link>
          <Link
            class="flex items-center mt-4 py-2 px-6 block border-l-4 border-gray-900 text-blue-500 hover:bg-blue-600  hover:text-white"
            to="/admin/feedback"
          >
            <i class="fas fa-comment-dots"></i>

            <span class="mx-4">Réclamations</span>
          </Link>
        </nav>
      </div>

      <div class="flex-1  bg-gray-100 ">
        <div class="flex-1 px-6 mb-4">
          <h1 class="mb-2 text-3xl text-gray-700">Tables</h1>

          {/**Routing Goes here */}
          <Switch>
            <AdminRoute exact path="/admin/products" component={Products} />
            <AdminRoute exact path="/admin/categories" component={Categories} />
            <AdminRoute exact path="/admin/profile" component={Profile} />
            <AdminRoute exact path="/admin/users" component={Users} />
            <AdminRoute exact path="/admin/transactions" component={Transactions}/>
            <AdminRoute exact path="/admin/settings" component={Settings} />
            <AdminRoute exact path="/admin/feedback" component={Feedback} />
            <AdminRoute exact path="/admin/bids" component={Bids} />
            <AdminRoute exact path="/admin/commandes" component={Commandes} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </div>
    </div>
  )
}

export default AdminDashboardRoutes
