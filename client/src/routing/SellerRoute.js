
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../components/components/Spinner';

const SellerRoute = ({
  component: Component,
  auth: { loading, user },
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      loading ? (
        <Spinner />
      ) : user.isSeller ? (
        <Component {...props} />
      ) : (
        <Redirect to="/" />
      )
    }
  />
);

SellerRoute.propTypes = {
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(SellerRoute);