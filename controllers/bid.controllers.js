const Bid = require( "../models/bid.models" );
const Product = require( "../models/product.models" );
const createBid = async ( req, res ) => {

	const product = req.product;
	const bid = new Bid( {
		bidder: req.body.bidder,
		priceBefore: req.body.priceBefore,
		bidMuch: req.body.bidMuch,
		product: product._id
	} );

	try {
		const savedBid = await bid.save();
		const searchProduct = await Product.findById( product._id );
		await searchProduct.updateLastBidPrice( savedBid.priceAfter );
		await searchProduct.addBidders( savedBid._id );
		return res.status( 200 ).json( { savedBid: savedBid, product: searchProduct } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getBids = async ( req, res ) =>{
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1 ;
	try {
		const count = await Bid.find().countDocuments();
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true: false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const bids = await Bid.find().skip( ( page - 1 ) * pagination ).limit( pagination ).populate( { path: "bidder", select:[ "firstName", "lastName", "phoneNumber" ] } ).populate( "product" ).sort( "field -createdAt" );
		return res.status( 200 ).json( { bids: bids, count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getBid = async ( req, res ) =>{
	const bid = req.bid;
	try{
		return res.status( 200 ).json( { bid: bid } );

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.createBid = createBid;
module.exports.getBids = getBids;
module.exports.getBid = getBid;