const Product = require( "../models/product.models" );
const moment = require( "moment" );
const createProduct = async ( req, res )=>{
	const product = new Product( {
		productName: req.body.productName,
		description: req.body.description,
		originalPrice: req.body.originalPrice,
		basePrice: req.body.basePrice,
		productImage: req.file.filename,
		quantity: req.body.quantity,
		category: req.body.category,
		seller: req.body.seller,
		startBidTime: req.body.startBidTime,
		endBidTime: req.body.endBidTime,
		bidAmount: req.body.bidAmount
	} );
	try {
		const savedProduct = await product.save();
		return res.status( 200 ).json( { savedProduct: savedProduct } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

//Get all products
const getProducts = async ( req, res ) => {
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1 ;
	try{
		const count = await Product.find().countDocuments();
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true: false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const products = await Product.find().skip( ( page - 1 ) * pagination ).limit( pagination ).populate( { path: "seller", select:[ "firstName", "lastName", "phoneNumber" ] } ).populate( "category" ).sort( "field -createdAt" );
		return res.status( 200 ).json( { products: products, count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } );
	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
};
const getTodayProducts = async ( req, res ) => {
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1 ;
	try{
		const count = await Product.find( { endBidTime: { $gte: moment().format( "YYYY-MM-DDTHH:mm:ss" ), $lte: moment().add( 1, "day" ).format( "YYYY-MM-DDTHH:mm:ss" ) } } ).countDocuments();
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true: false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const products = await Product.find( { endBidTime: { $gte :moment().format( "YYYY-MM-DDTHH:mm:ss" ), $lte: moment().add( 1, "day" ).format( "YYYY-MM-DDTHH:mm:ss" ) } } ).skip( ( page - 1 ) * pagination ).limit( pagination ).populate( { path: "seller", select:[ "firstName", "lastName", "phoneNumber" ] } ).populate( "category" ).sort( "field -createdAt" );
		return res.status( 200 ).json( { products: products, count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } );
	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getProductsByCategory = async ( req, res ) => {
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1 ;
	const category = req.category;
	try{
		const count = await Product.find( { category: category._id } ).countDocuments();
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true: false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const products = await Product.find( { category: category._id } ).skip( ( page - 1 ) * pagination ).limit( pagination ).populate( { path: "seller", select:[ "firstName", "lastName", "phoneNumber" ] } ).populate( "category" ).sort( "field -createdAt" );
		return res.status( 200 ).json( { products: products, count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } );
	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
};
//Get single product
const getProduct = async( req, res )=> {

	const product = req.product;
	try{
		return res.status( 200 ).json( { product: product } );

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
};
//update product
const updateProduct = async( req, res )=>{
	const product = req.product;
	try {
		const dataToUpdate = req.body;
		let { ...updateData } = dataToUpdate;
		if( req.file !== undefined ){
			updateData = { ...updateData, productImage:req.file.filename };
		}
		const updateProduct = await Product.findByIdAndUpdate( product._id, updateData, { new: true } );
		return res.status( 200 ).json( { updateProduct: updateProduct } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
//Get delete product
const deleteProduct = async( req, res )=>{
	try {
		const product = req.product;
		const deleteProduct = await Product.findByIdAndDelete( product._id );
		return res.status( 200 ).json( { deleteProduct: deleteProduct } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

module.exports.createProduct = createProduct;
module.exports.getProduct = getProduct;
module.exports.getProducts = getProducts;
module.exports.getTodayProducts = getTodayProducts;
module.exports.getProductsByCategory = getProductsByCategory;
module.exports.updateProduct = updateProduct;
module.exports.deleteProduct = deleteProduct;