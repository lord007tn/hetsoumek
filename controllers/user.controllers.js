const User = require( "../models/user.models" );


const currentUser = async( req, res ) => {

	try {
		const user = await User.findById( req.verifiedUser._id );
		return res.status( 200 ).json( { user: user.userProfile() } );
	} catch ( err ) {
		return res.status( 401 ).json( { err_message: err } );
	}
};
const getUsers = async( req, res )=>{
	const pagination = req.query.pagination ? parseInt( req.query.pagination ) : 100;
	const page = req.query.page ? parseInt( req.query.page ) : 1 ;
	try {
		const count = await User.find().countDocuments();
		const pages = ( pagination > 0 ) ? ( Math.ceil( count / pagination ) || 1 ) : null;
		const hasNextPage = ( page < pages ) ? true : false;
		const hasPreviousPage = ( page > 1 ) ? true: false;
		const nextPage = ( hasNextPage ) ? page + 1 : null;
		const previousPage = ( hasPreviousPage ) ? page - 1 : null;
		const users = await User.find().skip( ( page - 1 ) * pagination ).limit( pagination ).select( "-password" );
		return res.status( 200 ).json( { users: users, count: count, nextPage: nextPage, previousPage: previousPage, hasNextPage: hasNextPage, hasPreviousPage: hasPreviousPage, pages: pages, page: page } );
	
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
const getUser = async( req, res )=>{

	const id = req.user._id;

	try {
		const user = await User.findById( id );
		return res.status( 200 ).json( { user: user } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateUser = async( req, res )=>{
	const id = req.user._id;
	try {
		const dataToUpdate = req.body;
		let { ...updateData } = dataToUpdate;
		if( req.file !== undefined ){
			updateData = { ...updateData, avatar:req.file.filename };
		}
		const updateUser = await User.findOneAndUpdate( id, updateData, { new: true } );
		return res.status( 200 ).json( { updateUser: updateUser } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const deleteUser = async( req, res )=>{
	const id = req.user._id;

	try {
		const deleteUser = await User.findByIdAndDelete( id );
		return res.status( 200 ).json( { deleteUser: deleteUser } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.currentUser = currentUser;
module.exports.getUser = getUser;
module.exports.getUsers = getUsers;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;