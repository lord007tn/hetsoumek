const slug = require( "slug" );
const uniqueValidator = require( "mongoose-unique-validator" );
const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const ProductSchema = new Schema( {
	productName:{ type: String, min: 6, max: 255, index: true, required: true },
	slugName: { type: String, lowercase: true, unique: true },
	description:{ type: String, },
	originalPrice: { type: Number, required: true, default:0 },
	basePrice: { type: Number, required: true, default:0 },
	lastBidPrice:{ type: Number, default:0 },
	productImage: { type: String, default: "", required: true },
	quantity: { type: Number, required: true, default:0 },
	category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
	seller: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	startBidTime: { type: Date, required: true },
	endBidTime: { type: Date, required: true },
	bidders: [ { type: mongoose.Schema.Types.ObjectId, ref: "Bid" } ],
	state: { type: String, default: 0 }, //0 pending 1 sold 2 timeout
	bidAmount:{ type: Number, default:1 }
},{ timestamps: true } );

ProductSchema.plugin( uniqueValidator, { message: "is already taken" } );

ProductSchema.pre( "validate", function( next ){
	if( !this.slugName ){
		this.slugify();
	}
	if( !this.lastBidPrice ){
		this.lastBidPrice = this.basePrice;
	}
	next();
} );

ProductSchema.methods.slugify = function() {
	this.slugName = slug( this.productName ) + "-" + ( Math.random() * Math.pow( 36, 6 ) | 0 ).toString( 36 );
};

ProductSchema.methods.addBidders = function( id ) {
	this.bidders.push( id );
	return this.save();
};

ProductSchema.methods.updateLastBidPrice = function( bidPrice ) {
	this.lastBidPrice = bidPrice;
	return this.save();
};
module.exports = mongoose.model( "Product", ProductSchema ) || mongoose.models.Product;