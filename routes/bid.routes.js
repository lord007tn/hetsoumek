const router = require( "express" ).Router();
const Product = require( "../models/product.models" );
const Bid = require( "../models/bid.models" );
const verifyToken = require( "../utils/verifyToken" );
const isAdmin =require( "../utils/isAdmin" );
const bidControllers = require( "../controllers/bid.controllers" );

router.param( "product", async( req, res,next, id )=>{
	try{
		const product = await Product.findById( id );
		if( !product ) return res.sendStatus( 404 );
		req.product = product;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}

} );
router.param( "bid", async( req, res,next, id )=>{
	try{
		const bid = await Bid.findById( id );
		if( !bid ) return res.sendStatus( 404 );
		req.bid = bid;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}

} );
router.get( "/", verifyToken, isAdmin, bidControllers.getBids );
router.get( "/:bid", verifyToken, bidControllers.getBid );
router.post( "/:product/create", verifyToken, bidControllers.createBid );

module.exports = router;