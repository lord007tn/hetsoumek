const router = require( "express" ).Router();
const Category = require( "../models/category.models" );
const categoryControllers = require( "../controllers/category.controllers" );
const verifyToken = require( "../utils/verifyToken" );
const isAdmin =require( "../utils/isAdmin" );
router.param( "category", async( req, res,next, id )=>{
	try{
		const category = await Category.findById( id );
		if( !category ) return res.sendStatus( 404 );
		req.category = category;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}

} );

router.get( "/", categoryControllers.getCategories );
router.get( "/:category", categoryControllers.getCategory );
router.post( "/create", verifyToken, isAdmin, categoryControllers.createCategory );
router.put( "/:category/update", verifyToken, isAdmin, categoryControllers.updateCategory );
router.delete( "/:category/delete", verifyToken, isAdmin, categoryControllers.deleteCategory );
module.exports = router;