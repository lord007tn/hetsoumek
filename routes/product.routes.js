const router = require( "express" ).Router();
const productControllers = require( "../controllers/product.controllers" );
const Product = require( "../models/product.models" );
const Category = require( "../models/category.models" );
const verifyToken = require( "../utils/verifyToken" );
const isSeller = require( "../utils/isSeller" );
const multer = require( "multer" );

const storage = multer.diskStorage( {
	destination: function( req, file, cb ) {
		cb( null, "uploads" );
	},
	filename: function( req, file, cb ) {
		cb( null, new Date().toISOString().replace( /[/\\:]/g, "_" ) + file.originalname );
	}
} );

const fileFilter = ( req, file, cb ) => {
	// reject a file
	if ( file.mimetype === "image/jpeg" || file.mimetype === "image/png" ) {
		cb( null, true );
	} else {
		cb( null, false );
	}
};

const upload = multer( {
	storage: storage,
	fileFilter: fileFilter,
	onFileUploadStart: function ( file ) {
		console.log( file.originalname + " is starting ..." );}
} );

router.param( "product", async( req, res, next, id )=>{
	try{
		const product = await Product.findById( id ).populate("category");
		if( !product ) return res.sendStatus( 404 );
		req.product = product;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.param( "category", async( req, res,next, name )=>{
	try{
		const category = await Category.findOne( { categoryName: name } );
		if( !category ) return res.sendStatus( 404 );
		req.category = category;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.get( "/today", productControllers.getTodayProducts );
router.get( "/", productControllers.getProducts );
router.get( "/:category/products", productControllers.getProductsByCategory );
router.post( "/create", upload.single( "productImage" ), verifyToken, isSeller, productControllers.createProduct );
router.get( "/:product", productControllers.getProduct );
router.put( "/:product/update", upload.single( "productImage" ), verifyToken, isSeller, productControllers.updateProduct );
router.delete( "/:product/delete", verifyToken, isSeller, productControllers.deleteProduct );

module.exports = router;